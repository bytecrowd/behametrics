## Behametrics
#### Prerequisites
1. Min SDK version 19
2. Java 8

### Installation
##### Gradle
Add maven url into your root ```build.gradle``` file
```
allprojects {
    repositories {
        ...
        maven { url "https://jitpack.io" }
    }
}
```

Then add this into your ```app/build.gradle``` file
```
dependencies {
    compile 'com.gitlab.bytecrowd:logger:vX.X.X'
    compile 'com.gitlab.bytecrowd:logger:processor:vX.X.X'
    compile 'com.gitlab.bytecrowd:logger:authentication:vX.X.X'
    compile 'com.gitlab.bytecrowd.logger:annotations:vX.X.X'
    annotationProcessor 'com.gitlab.bytecrowd:logger:processor:vX.X.X'
}
```


## Anotation definition
### Usage
If you want to log in specific activity follow these steps :
1. Mark your class with ```@Logger()``` annotation
2. Put the [ClassName]Logger.init(this) in onCreate method
3. Build App project 
4. Start App project 

Example : 
```java

@Logger(...) // Add annotation before class definition
public class ClassName extends Activity
{
    public void onCreate(Bundle savedInstanceState)
    {
        ClassNameLogger.init(this); // You need to add this before super.onCreate
        super.onCreate(savedInstanceState);
    }
}

```
### Alternative
We highly don't recommend this, but it's possible to do logging by alternatively calling Behametrics methods (see the example below) :
```java
public class ClassName extends Activity
{
	@Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        BehametricsInstance.start(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BehametricsInstance.resume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BehametricsInstance.destroy(this);
    }

    protected void onStop() {
        super.onStop();
    }
}
```
## Dialogs
If you're creating dialogs at the runtime of applications, 
it's necessary to add following statement :
``` 
BehametricsInstance.catchTouchEvent(view, event)
```
into your dialog-component listener


### Customize configuration

Add ```config.properties``` file into assets folder.

Default Content of ```config.properties``` :
`
JsonKeyFile=fileToUpload
JsonKeyDevice=uuid
JsonKeyDirName=dirname
ServerUrl=http://147.175.149.155:8000
SocketAddress=http://147.175.149.155:8000/
ModuleAuthentication=True
LogFilesPath=/behametrics-sensor-logs
FileMaxRows=1000
MinFreeSpaceLimitMB=300
defaultDirName=default
ClientId=bytecrowd`
