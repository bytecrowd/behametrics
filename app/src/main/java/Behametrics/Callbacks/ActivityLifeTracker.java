package Behametrics.Callbacks;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import Behametrics.BehametricsInstance;

/**
 * Created by Imothep on 09.02.2018.
 */

public class ActivityLifeTracker implements Application.ActivityLifecycleCallbacks {


    public ActivityLifeTracker()
    {

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.d("Created Activity", activity.getLocalClassName());
        BehametricsInstance.create(activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.d("Started Activity", activity.getLocalClassName());
        BehametricsInstance.start(activity);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.d("Resumed Activity", activity.getLocalClassName());
        BehametricsInstance.resume(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.d("Destroyed Activity", activity.getLocalClassName());
        activity.getApplication().unregisterActivityLifecycleCallbacks(this);
        BehametricsInstance.destroy(activity);
    }
}
