package Behametrics.Utils.Network;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;

import java.io.File;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import Behametrics.BehametricsInstance;
import Behametrics.SensorLogger.DataTypes.AbstractSample;
import Behametrics.Utils.Observe;
import Behametrics.Utils.PropertyReader;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Class for functions using internet connections
 */
public class NetworkUtils extends Observe<PropertyReader> {
    private static final String TAG = "NetworkUtils";

    private ConnectivityManager connectivityManager;
    private ServiceGenerator serviceGenerator;
    private SharedPreferences wmbPreference;
    private Socket socket;

    public NetworkUtils(Context context, PropertyReader propertyReader) {
        super(propertyReader);
        subscribe();
        connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        serviceGenerator = new ServiceGenerator();
        wmbPreference = PreferenceManager.getDefaultSharedPreferences(context);

        String streamAddress = target.getStringProperty("StreamAddress");

        if(!target.getStringProperty("StreamOption").equals("False")){
           Log.d("STREAM", "OK");

            try {
                socket = IO.socket(streamAddress);
            } catch (URISyntaxException e) {
                Log.e("NetworkUtils", "Socket error - not open");
            }
            socket.connect();
        }

    }

    /**
     * Function for stream data to server in json format
     * @param sample
     */
    public void stream(AbstractSample sample){
        socket.emit("streaming", sample.serializeToJson());
    }


    /**
     * Function for sending file
     *
     * @param serverUrl URL address of server = destination of file
     * @param file      File to send
     * @param callback  callback to handle response
     */
    public void sendFile(String serverUrl, File file, Callback<okhttp3.ResponseBody> callback) {
        serviceGenerator.setBaseUrl(serverUrl);

        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        file
                );

        String newName = mapName(file.getName());
        MultipartBody.Part body =
                MultipartBody.Part.createFormData(target.getStringProperty("JsonKeyFile"), newName, requestFile);

        Map<String, String> headers = new HashMap<>();
        headers.put(target.getStringProperty("JsonKeyDevice"), getDeviceUUID());
        headers.put(target.getStringProperty("JsonKeyDirName"), newName);

        if (serverUrl.toLowerCase().startsWith("https"))
            headers.put("clientid", target.getStringProperty("ClientId"));

        Call<ResponseBody> call = service.uploadFile(headers, body);
        call.enqueue(callback);
    }

    /**
     * Function for getting info about network connectivity
     * @return info about connection
     */
    public boolean isOnline() {
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    /**
     * Function for getting UUID of instalation for unique identity
     *
     * @return UUID as unique instance of instalation
     */
    public String getDeviceUUID() {
        String UUID = wmbPreference.getString("UUID", null);
        return UUID;
    }

    private String mapName(String name) {
        String newName = new String();
        int numOfMinus = 0;
        char currentChar;
        for(int i = 0; i < name.length(); i++) {

            currentChar = name.charAt(i);
            if(currentChar == '-')
            {
                numOfMinus++;

                if(numOfMinus > 1)
                {
                    newName += "-";
                    numOfMinus = 0;
                }
            }
            else
            {
                if(numOfMinus == 1)
                {
                    newName += "/";

                    numOfMinus = 0;
                }

                newName += currentChar;
            }
        }

        return newName;
    }

    @Override
    public void update(PropertyReader obj) {
        target = obj.copy();
    }

    @Override
    public void subscribe() {
        BehametricsInstance.addToList(this);
    }

}