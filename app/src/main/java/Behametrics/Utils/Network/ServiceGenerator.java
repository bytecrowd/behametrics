package Behametrics.Utils.Network;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.Certificate;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import Behametrics.BehametricsInstance;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Generator for internet services
 */
public class ServiceGenerator {

    private static String baseUrl;
    private static Retrofit.Builder builder;
    private static Retrofit retrofit;
    private static OkHttpClient httpClient;
    private static final String TAG = "ServiceGenerator";

    ServiceGenerator() {
    }

    final class SecurityCertificate {
        SSLSocketFactory sslSocketFactory;
        X509TrustManager trustManager;

        SecurityCertificate(SSLSocketFactory sslSocketFactory,X509TrustManager trustManager){
            this.sslSocketFactory=sslSocketFactory;
            this.trustManager=trustManager;
        }
    }

    /**
     * Function to create secure socket
     * @return SSL Socket
     */
    private SecurityCertificate getSSLSocket(){
        TrustManagerFactory trustManagerFactory;
        TrustManager[] trustManagers;
        X509TrustManager trustManager = null;
        SSLSocketFactory sslSocketFactory = null;
        SSLContext sslContext;

        //load keystore certificate from file
        Context context = BehametricsInstance.getContext();
        int CertID = context.getResources().getIdentifier("raw/cert", null, context.getPackageName());
        InputStream inStream  =context.getResources().openRawResource(CertID);
        KeyStore ks=null;
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate cert = cf.generateCertificate(inStream);
            inStream.close();
            String keyStoreType = KeyStore.getDefaultType();
            ks = KeyStore.getInstance(keyStoreType);
            ks.load(null, null);
            ks.setCertificateEntry("cert", cert);
        }catch(CertificateException e){
            Log.e(TAG, "Error creating certificate", e);
        }catch(IOException e){
            Log.e(TAG, "Error reading the certificate file", e);
        }catch(KeyStoreException e){
            Log.e(TAG, "Error creating the key store", e);
        }catch(NoSuchAlgorithmException e){
            Log.e(TAG, "Error loading the key store", e);
        }

        try {
            trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) ks);
            trustManagers = trustManagerFactory.getTrustManagers();

            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
            }
            trustManager = (X509TrustManager) trustManagers[0];

            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagers, new java.security.SecureRandom());
            sslSocketFactory = sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            Log.e(TAG, "Error creating SSL Socket", e);
        }
        return new SecurityCertificate(sslSocketFactory, trustManager);
    }

    /**
     * Set URL by param newBaseUrl and set connection to server
     * @param newBaseUrl URL address of server
     */
    public void setBaseUrl(String newBaseUrl) {
        if (baseUrl == null || baseUrl.equals(newBaseUrl)) {
            baseUrl = newBaseUrl;

            httpClient = new OkHttpClient().newBuilder()
                    //.sslSocketFactory(getSSLSocket().sslSocketFactory, getSSLSocket().trustManager)
                    //.hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .build();

            builder = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .baseUrl(baseUrl);
            retrofit = builder.build();
        }
    }

    /**
     * Set connection to service by param serviceClass
     * @param serviceClass Interface of service where are stored functions for connection
     * @param <S> Object
     * @return Service by serviceClass
     */
    public static <S> S createService(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}