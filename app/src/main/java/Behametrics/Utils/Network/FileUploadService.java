package Behametrics.Utils.Network;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Service as Interface for definitions of Rest functions
 */
public interface FileUploadService {

    @Multipart
    @POST("./")
    Call<ResponseBody> uploadFile(
            @HeaderMap Map<String, String> headers,
            @Part MultipartBody.Part file
    );
}
