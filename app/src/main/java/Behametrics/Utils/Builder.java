package Behametrics.Utils;

/**
 * Created by Peter on 08/Feb/18.
 */

public abstract class Builder<T>{
    public abstract T copy();
}
