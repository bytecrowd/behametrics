package Behametrics.Utils;

/**
 * Created by Peter on 08/Feb/18.
 */

public abstract class Observe <T extends Builder<T>> {
    protected T target;
    protected Observe(T t)
    {
        target = t.copy();
    }

    /**
     * Method used for updating object
     * @param obj
     */
    public abstract void update(T obj);

    /**
     * Method used for subscribe on other class
     */
    public abstract void subscribe();
}
