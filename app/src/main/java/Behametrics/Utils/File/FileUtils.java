package Behametrics.Utils.File;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import Behametrics.BehametricsInstance;
import Behametrics.Utils.Observe;
import Behametrics.Utils.PropertyReader;

/**
 * FileUtils
 */
public class FileUtils extends Observe<PropertyReader> implements Closeable{

    private String TAG = "FileUtils";
    private BufferedWriter bufferedWriter;
    private AtomicInteger numOfFilesToSend = new AtomicInteger(0);
    private int actRowNumber;
    private String contextFilesDir;
    private boolean sendingLock;

    /**
     * Constructor for FileUtils.
     *
     * @param contextFilesDir
     * @throws IOException
     * @throws OutOfMemoryError
     */
    public FileUtils(String contextFilesDir, PropertyReader propertyReader) throws IOException, OutOfMemoryError {
        super(propertyReader);
        subscribe();

        this.contextFilesDir = contextFilesDir;
        removeEmptyFiles();
        numOfFilesToSend.set(getExistingFilesCount());
        actRowNumber = 0;
        sendingLock = false;
        createLogFile();
    }

    /**
     * FileUtils constructor for testing purposes.
     *
     * @param bufferedWriter
     */
    public FileUtils(BufferedWriter bufferedWriter) {
        super(new PropertyReader(null));
        this.bufferedWriter = bufferedWriter;
    }

    /**
     * Write single row to actual logfile.
     *
     * @param row
     * @throws IOException
     * @throws OutOfMemoryError
     */
    public void write(String row) throws IOException, OutOfMemoryError {
        if (bufferedWriter == null) {
            createLogFile();
        }

        if (bufferedWriter != null) {
            bufferedWriter.write(row);
            actRowNumber++;
            if (actRowNumber >= target.getIntProperty("FileMaxRows")) {
                nextFile();
            }
        }
    }

    /**
     * Delete logfile which was already sended.
     *
     * @return
     * @throws IOException
     */
    public boolean delete() throws IOException {
        File file = getFileToSend();
        Log.d(TAG, "Vymazavam subor " + file.getName());
        numOfFilesToSend.decrementAndGet();
        return file.delete();
    }

    /**
     * Returns non-empty file to send. If no file exists, returns null.
     *
     * @return
     */
    public File getFileToSend() {
        File[] files = new File(contextFilesDir + target.getStringProperty("LogFilesPath")).listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });

        if (files.length == 0)
            return null;

        long firstMod = Long.MIN_VALUE;
        File choice = null;
        for (File file : files) {
            if (file.length() > firstMod && file.length() != 0) {
                choice = file;
                firstMod = file.length();
            }
        }
        return choice;
    }

    /**
     * Returns true if file to send exists, false otherwise.
     *
     * @return
     */
    public boolean existFileToSend() {
        return !sendingLock && numOfFilesToSend.get() > 0;
    }

    /**
     * Closes underlying BufferedWriter.
     *
     * @throws IOException
     */
    @Override
    public void close() throws IOException {
        bufferedWriter.close();
    }

    /**
     * Closes actual file and creates new logfile.
     *
     * @throws IOException
     * @throws OutOfMemoryError
     */
    private void nextFile() throws IOException, OutOfMemoryError {
        bufferedWriter.close();
        bufferedWriter = null;
        actRowNumber = 0;
        numOfFilesToSend.incrementAndGet();
        createLogFile();
    }

    public int getActRowNumber() {
        return actRowNumber;
    }

    public void setActRowNumber(int actRowNumber) {
        this.actRowNumber = actRowNumber;
    }

    public void setSendingLock(boolean sendingLock) {
        this.sendingLock = sendingLock;
    }

    public int getNumOfFilesToSend() {
        return numOfFilesToSend.get();
    }

    /**
     * Creates new flowfile. Throws {@link OutOfMemoryError} if specified free space limit is exceeded.
     *
     * @throws IOException
     * @throws OutOfMemoryError
     */
    private void createLogFile() throws IOException, OutOfMemoryError {
        Long usableSpaceMB = getUsableSpaceMB();
        Log.d(TAG, "Volne miesto " + Long.toString(usableSpaceMB));
        if (usableSpaceMB > target.getIntProperty("MinFreeSpaceLimitMB")) {
            Long timestamp = System.currentTimeMillis();
            Log.d(TAG, "Vytvaram subor " + target.getStringProperty("defaultDirName") + "-" + Long.toString(timestamp) + ".csv");
            FileWriter fileWriter = new FileWriter(contextFilesDir + target.getStringProperty("LogFilesPath") + "/" + target.getStringProperty("defaultDirName") + "-" + Long.toString(timestamp) + ".csv", true);
            bufferedWriter = new BufferedWriter(fileWriter);
        } else {
            throw new OutOfMemoryError("Minimal specified free space limit " + target.getIntProperty("MinFreeSpaceLimitMB") + "MB exceeded");
        }
    }

    /**
     * Returns count of existing non-empty files.
     *
     * @return
     * @throws IOException
     */
    private int getExistingFilesCount() throws IOException {
        File dir = new File(contextFilesDir + target.getStringProperty("LogFilesPath"));
        dir.mkdirs();

        File[] files = new File(contextFilesDir + target.getStringProperty("LogFilesPath")).listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });

        if (files == null) {
            return 0;
        }

        return files.length;
    }

    /**
     * Removes existing empty files.
     */
    private void removeEmptyFiles() {
        File dir = new File(contextFilesDir + target.getStringProperty("LogFilesPath"));
        dir.mkdirs();

        File[] files = new File(contextFilesDir + target.getStringProperty("LogFilesPath")).listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });

        for (File file : files) {
            if (file.length() == 0) {
                Log.d(TAG, "Vymazavam prazdny subor");
                file.delete();
            }
        }
    }

    /**
     * Returns usable space estimation.
     *
     * @return
     */
    private long getUsableSpaceMB() {
        return new File(contextFilesDir + target.getStringProperty("LogFilesPath")).getUsableSpace() / (1024 * 1024);
    }

    @Override
    public void update(PropertyReader obj) {
        target = obj.copy();
    }

    @Override
    public void subscribe() {
        BehametricsInstance.addToList(this);
    }
}
