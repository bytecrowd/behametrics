package Behametrics.Utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Class for Properties in lib
 */
public class PropertyReader extends Builder<PropertyReader> {
    private Context context;
    private Properties properties;
    private static final String TAG = "PropertyReader";
   // private HashMap<String, String> propertiesMap;

    /**
     * Constructur to set context and empty properties
     * @param context Context of application
     */
    public PropertyReader(Context context) {
        this.context = context;
        properties = new Properties();
      //  propertiesMap = new HashMap<String, String>();
    }

    public void setProperty(String name, String value)
    {
        properties.setProperty(name, value);
    }

    /**
     * Function to set properties by file stored in assets
     * @param FileName Name of file where are settings stored
     */
    public void setProperties(String FileName) {
        try {
            AssetManager assetManager = context.getAssets();
            InputStream inputStream = assetManager.open(FileName);
            properties.load(inputStream);
/*
            Enumeration p = properties.propertyNames();

            while (p.hasMoreElements()) {
                String key = (String) p.nextElement();
                propertiesMap.put(key, properties.getProperty(key));
            }*/


        } catch (IOException e) {
            Log.e(TAG, "Error setting properties",e);
        }
    }

    /**
     * Function for getting string Properties
     * @param key Key of Property
     * @return Value of Property
     */
    public  String getStringProperty(String key){
        return properties.getProperty(key);
    }

    /**
     * Function for getting Integer Properties
     * @param key Key of Property
     * @return Value of Property converted to Integer
     */
    public int getIntProperty(String key){
        return properties.getProperty(key) != null ? Integer.parseInt(properties.getProperty(key)) : 0;
    }

    @Override
    public PropertyReader copy() {
        PropertyReader newPropertyReader = new PropertyReader(context);
        Enumeration p = properties.propertyNames();

        while (p.hasMoreElements()) {
            String key = (String) p.nextElement();
            newPropertyReader.setProperty(key, properties.getProperty(key));
        }

        return newPropertyReader;
    }
}
