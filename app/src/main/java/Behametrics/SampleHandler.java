package Behametrics;

import android.content.Context;
import android.util.Log;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Behametrics.Handlers.AbstractHandler;
import Behametrics.Handlers.CsvHandler;
import Behametrics.Handlers.StreamHandler;
import Behametrics.SensorLogger.DataTypes.AbstractSample;
import Behametrics.Utils.File.FileUtils;
import Behametrics.Utils.Network.NetworkUtils;
import Behametrics.Utils.Observe;
import Behametrics.Utils.PropertyReader;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * SampleHandler
 */

public class SampleHandler extends Observe<PropertyReader> implements Closeable {

    private static final String TAG = "SampleHandler";
    private NetworkUtils networkUtils;
    private FileUtils fileUtils;
    private String contextFilesDir;
    private Thread thread;
    private List<AbstractHandler> handlers;

    /**
     * Sample handler constructor.
     *
     * @param contextFilesDir
     */
    public SampleHandler(String contextFilesDir, Context context, PropertyReader propertyReader) {
        super(propertyReader);
        subscribe();

        this.contextFilesDir = contextFilesDir;
        try {
            fileUtils = new FileUtils(this.contextFilesDir, propertyReader);
        } catch (IOException | OutOfMemoryError e) {
            Log.e(TAG, "Chyba pri vytvarani FileUtils: ", e);
        }
        networkUtils = new NetworkUtils(context, propertyReader);
        runBackround();

        String streaming = target.getStringProperty("StreamOption");

        handlers = new ArrayList<>();

        if(networkUtils.isOnline() && streaming.equals("True")) {
            handlers.add(new StreamHandler(networkUtils));
        } else if(streaming.equals("False")) {
            handlers.add(new CsvHandler(fileUtils));
        }else {
            handlers.add(new StreamHandler(networkUtils));
            handlers.add(new CsvHandler(fileUtils));
        }

    }

    /**
     * Callback to handle responses from server.
     */
    Callback callback = new Callback<ResponseBody>() {

        @Override
        public void onResponse(Call<ResponseBody> call,
                               Response<ResponseBody> response) {
            Log.e(TAG, "Response Code " + response.code());
            if (response.code() == 200) {
                Log.i(TAG, "Upload success");
                try {
                    fileUtils.delete();
                } catch (IOException e) {
                    Log.e(TAG, "Chyba pri vymazavani odoslaneho suboru: ", e);
                }
            } else {
                Log.e(TAG, "Upload error response: " + response.message());
            }
            fileUtils.setSendingLock(false);
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            Log.e(TAG, "Upload error:", t);
            fileUtils.setSendingLock(false);
        }
    };

    /**
     * Add multiple samples to file utils.
     *
     * @param samples
     * @return
     */
    public boolean addAllSamples(List<AbstractSample> samples) {
        for (AbstractSample sample : samples) {
            try {
                if (fileUtils == null) {
                    fileUtils = new FileUtils(this.contextFilesDir, target);
                }
                fileUtils.write(sample.serializeToCsv());
            } catch (IOException | OutOfMemoryError e) {
                Log.e(TAG, "Chyba pri zapisovani do suboru: ", e);
                return false;
            }
        }
      /*  if (fileUtils.existFileToSend()) {
            sendFile();
        }*/
        return true;
    }

    /**
     * Add single sample to flowfile.
     *
     * @param sample
     * @return
     */
    public boolean addSample(AbstractSample sample) {
        sample.setUserId(networkUtils.getDeviceUUID() + "/" + target.getStringProperty("defaultDirName"));
        //return handler.handle(sample);

        for (AbstractHandler handler: handlers) {
            handler.handle(sample);
        }

        return true;
    }

    /**
     * Closes underlying FileUtils instance.
     */
    @Override
    public void close() {
        try {
            fileUtils.close();
        } catch (IOException e) {
            Log.e(TAG, "Chyba pri zatvarani FileUtils: ", e);
        }
    }

    /**
     * Sends file received from FileUtils.
     */
    private void sendFile() {
        Log.e(TAG, "Posielam subor" + fileUtils.getFileToSend().getName());
       // if (networkUtils.isOnline()) {
            fileUtils.setSendingLock(true);
            networkUtils.sendFile(target.getStringProperty("ServerUrl"),
                    fileUtils.getFileToSend(),
                    callback);
       // }
    }

    public FileUtils get(){
        return fileUtils;
    }

    @Override
    public void update(PropertyReader obj) {
        target = obj.copy();
    }

    @Override
    public void subscribe() {
        BehametricsInstance.addToList(this);
    }

    private void runBackround(){

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    if (networkUtils.isOnline()) {
                        if(fileUtils.existFileToSend()) {
                            Log.d("NumOfFilesToSend", String.valueOf(fileUtils.getNumOfFilesToSend()));
                            sendFile();
                        }
                    }else {
                        try {
                            Thread.sleep(5000);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }
        });
        thread.start();
    }



}
