package Behametrics.SensorLogger;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import Behametrics.SampleHandler;
import Behametrics.SensorLogger.DataTypes.InternalRawSample;

/**
 * Creates logger which is enable to measure data from touch events
 * providing them to SampleHandler Class
 *
 * @author Imothep
 * @version 0.2.3
 * @see SampleHandler
 */
public class TouchHandler implements LoggerInterface {

    private Activity context;
    private SampleHandler sampleHandler;
    private long upTime = System.currentTimeMillis() - SystemClock.uptimeMillis();
    /**
     * Class constructor
     */
    public TouchHandler(SampleHandler sampleHandler) {
        setSampleHandler(sampleHandler);
    }


    /**
     * Class constructor
     *
     * @param context       context of activity
     * @param sampleHandler
     */
    public TouchHandler(Activity context, SampleHandler sampleHandler) {
        setContext(context);
        setSampleHandler(sampleHandler);
    }



    public boolean catchTouchEvent(View view, MotionEvent event)
    {
        synchronized(this) {
            int action = event.getActionMasked();

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    captureDown(event);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    captureUp(event);
                    break;
                case MotionEvent.ACTION_MOVE:
                    capturePointerMoves(event);
                    break;
            }

            return true;
        }
    }

    /**
     * On Touch event which handles every touch action on root view
     *
     * @param view  view in which event occurred
     * @param event raw-atomic sample measured by event
     * @return continue to track
     */


    /**
     * Captures down event, it's pointer initial state
     *
     * @param event
     */
    private void captureDown(MotionEvent event) {
        int index = event.getActionIndex();
        int id = event.getPointerId(index);

        Log.d("Down ", "ID :" + id + " COORDS : "+event.getX(index)+","+event.getY(index));
        addSample(event, "DOWN", id, index, MotionEvent.ACTION_DOWN, event.getPressure(index), event.getSize(index));

    }

    /**
     * Captures up event, it's pointer ending state
     *
     * @param event
     */
    private void captureUp(MotionEvent event) {
        int index = event.getActionIndex();
        int id = event.getPointerId(index);

        Log.d("UP ", "ID :" + id + " COORDS : "+event.getX(index)+","+event.getY(index));
        addSample(event, "UP", id, index, MotionEvent.ACTION_UP, event.getPressure(index), event.getSize(index));
    }

    /**
     * Captures move (drag) event, it's pointer continuous state
     *
     * @param event
     */
    private void capturePointerMoves(MotionEvent event) {
        int length = event.getPointerCount();
        int id;
        for (int i = 0; i < length; i++) {
            id = event.getPointerId(i);
            try {

                Log.d("MOVE ", "ID :" + id + " COORDS : "+event.getX(i)+","+event.getY(i));
                addSample(event, "MOVE", id, i, MotionEvent.ACTION_MOVE, event.getPressure(i), event.getSize(i));


            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    /**
     * Add measurement contained in event to sampleHandler
     *
     * @param event  event
     * @param id     id of pointer
     * @param index  index of event
     * @param action action of event
     * @param pressure pressure of touch event
     */
    private void addSample(MotionEvent event,String eventTypeName, int id, int index, int action, float pressure, float size) {

        if(sampleHandler == null) {
            return;
        }

        long timestamp = (upTime + event.getEventTime())*1000000;
        Log.d("Touch event - timestamp", timestamp + " ID : " + id);

        InternalRawSample sample = new InternalRawSample();
        sample.setTouchPointer(id);
        sample.setValues(new float[]{event.getX(index), event.getY(index)});
        sample.setTimestamp(timestamp);
        sample.setEventType(action);
        sample.setPressure(pressure);
        sample.setSize(size);
        sample.setEventTypeName(eventTypeName);
        sampleHandler.addSample(sample);

    }

    /**
     * Unregister all events
     *
     * @return successfully unregistered = true
     * @throws NullPointerException
     */
    public boolean unregisterAllEvents() {


        return true;
    }

    /**
     * Register all events
     *
     * @return successfully registered = true
     * @throws NullPointerException
     */
    public boolean registerAllEvents() {

        //Activity is base class of all type of activities so it should be working


        return true;
    }

    /**
     * Destroys all references and allocated resources
     * <p>
     * This behavior is recommended to keep track of internal associations
     * </p>
     */
    public void Destroy() {
        unregisterAllEvents();
        setSampleHandler(null);
        setContext(null);
    }

    /**
     * Sets current context to operate in it
     *
     * @param context context of activity
     */
    @Override
    public void setContext(Activity context) {
        this.context = context;
    }

    /**
     * Sets SampleHandler instance
     *
     * @param sampleHandler
     */
    @Override
    public void setSampleHandler(SampleHandler sampleHandler) {
        this.sampleHandler = sampleHandler;
    }

}
