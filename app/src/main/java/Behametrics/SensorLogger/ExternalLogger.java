package Behametrics.SensorLogger;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import Behametrics.SampleHandler;
import Behametrics.SensorLogger.DataTypes.ExternalRawSample;

/**
 * Creates logger which is enable to measure data from all available sensors
 * providing them to SampleHandler Class
 *
 * @author Imothep
 * @version 0.2.3
 * @see SampleHandler
 */
public class ExternalLogger implements SensorEventListener, LoggerInterface {

    private Collection<ExternalRawSample> samples;
    private SampleHandler sampleHandler;
    private Activity context;
    private long upTime = (System.currentTimeMillis()*1000000) - SystemClock.elapsedRealtimeNanos();
    /**
     * Event listener for onTouch events
     * <p>
     * This method is providing measurements to sampleHandler for logging
     * </p>
     *
     * @param sensorEvent raw-atomic sample measured by sensor
     * @see SensorEvent
     * @see SampleHandler
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        if(sampleHandler != null) {
            long timestamp = upTime + sensorEvent.timestamp;
            Log.d("Sensor - timestamp", timestamp + " ");

            sampleHandler.addSample(new ExternalRawSample(
                    sensorEvent.sensor.getType(),
                    sensorEvent.sensor.getName(),
                    timestamp,
                    sensorEvent.values)
            );
        }
    }

    /**
     * Event listener providing information about accuracy changes of specific sensor
     *
     * @param sensor specific sensor type
     * @param i      quality of accuracy
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    /**
     * Class constructor
     */
    public ExternalLogger() {
    }

    public boolean initLogger() {
        initSampleBuffer();
        registerAllSensors();

        return true;
    }

    /**
     * Class constructor specifying context to operate on
     *
     * @param context       context of activity
     * @param sampleHandler
     */
    public ExternalLogger(Activity context, SampleHandler sampleHandler) {
        setContext(context);
        setSampleHandler(sampleHandler);
    }

    public boolean run() {
        initSampleBuffer();
        registerAllSensors();

        return true;
    }

    /**
     * Initialize ExternalRawSample collection to keep samples for future processing
     *
     * @see ExternalRawSample
     */
    private void initSampleBuffer() {
        samples = Collections.synchronizedList(new ArrayList<ExternalRawSample>());
    }

    /**
     * Sets current context to operate in it
     *
     * @param context context of activity
     */
    @Override
    public void setContext(Activity context) {
        this.context = context;
    }

    /**
     * Sets SampleHandler instance
     *
     * @param sampleHandler
     */
    @Override
    public void setSampleHandler(SampleHandler sampleHandler) {
        this.sampleHandler = sampleHandler;
    }

    /**
     * Gets Collection of measured raw samples
     *
     * @return collection of sensor/s samples
     */
    public Collection<ExternalRawSample> getSamples() {
        return samples;
    }

    /**
     * Register all available sensors on current device
     *
     * @return successfully registered = true
     * @throws NullPointerException
     */
    public boolean registerAllSensors() throws NullPointerException {
        registerAllSensors(SensorManager.SENSOR_DELAY_GAME);
        return true;
    }

    /**
     * Register all available sensors on current device
     *
     * @param speed frequency of sensors measurements
     * @return successfully registered = true
     * @throws NullPointerException
     */
    public boolean registerAllSensors(int speed) throws NullPointerException {

        if (context == null) {
            throw new NullPointerException();
        }

        SensorManager manager = getSensorManager();
        List<Sensor> sensors = manager.getSensorList(Sensor.TYPE_ALL);

        for (Sensor s : sensors) {
            int type = s.getType();
            manager.registerListener(this, manager.getDefaultSensor(type), speed);
        }

        return true;
    }

    /**
     * Unregister all available sensors on current device
     *
     * @return successfully unregistered = true
     * @throws NullPointerException
     */
    public boolean unregisterAllSensors() throws NullPointerException {

        if (context == null) {
            throw new NullPointerException();
        }

        SensorManager manager = getSensorManager();
        List<Sensor> sensors = manager.getSensorList(Sensor.TYPE_ALL);

        for (Sensor s : sensors) {
            int type = s.getType();
            manager.unregisterListener(this, s);
        }

        return true;
    }

    /**
     * Register specific type of sensor
     *
     * @param type specific sensor to register
     * @return successfully registered = true
     * @throws NullPointerException
     */
    public boolean registerSensor(int type) throws NullPointerException {
        registerSensor(type, SensorManager.SENSOR_DELAY_GAME);
        return true;
    }

    /**
     * Register specific type of sensor with speed as additional parameter
     *
     * @param type  specific sensor to register
     * @param speed frequency of sensors measurements
     * @return successfully registered = true
     * @throws NullPointerException
     */
    public boolean registerSensor(int type, int speed) throws NullPointerException {

        if (context != null) {
            SensorManager manager = getSensorManager();
            manager.registerListener(this, manager.getDefaultSensor(type), speed);
            return true;
        }
        throw new NullPointerException();
    }

    /**
     * Unregister specific type of sensor
     *
     * @param type specific sensor to unregister
     * @return successfully unregistered = true
     * @throws NullPointerException
     */
    public boolean unregisterSensor(int type) throws NullPointerException {

        if (context != null) {
            SensorManager manager = getSensorManager();
            manager.unregisterListener(this, manager.getDefaultSensor(type));
            return true;
        }

        throw new NullPointerException();
    }

    /**
     * Gets @SensorManager which contain set of available sensors
     *
     * @return SensorManager
     * @throws NullPointerException
     * @see SensorManager
     */
    private SensorManager getSensorManager() throws NullPointerException {

        if (context != null) {
            return (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        }

        throw new NullPointerException();
    }

    /**
     * Destroys all references and allocated resources
     * <p>
     * This behavior is recommended to keep track of internal associations
     * </p>
     */
    public void Destroy() {
        unregisterAllSensors();
        setSampleHandler(null);
        setContext(null);
        samples.clear();
    }
}
