package Behametrics.SensorLogger;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import android.view.View;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;

import Behametrics.SensorLogger.TouchHandler;


public class InternalLogger implements Callback {

    private Activity activity;
    private Window.Callback callback;
    private TouchHandler touchHandler;

    public InternalLogger(TouchHandler touchHandler, Activity activity)
    {
        this.activity = activity;
        this.callback = activity.getWindow().getCallback();
        this.touchHandler = touchHandler;
    }

    public void Destroy()
    {
        // Grabage collected
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return callback.dispatchKeyEvent(keyEvent);
    }

    @Override
    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        return callback.dispatchKeyShortcutEvent(keyEvent);
    }

    // Trick to handle all of the views in activity (dynamic and static views)
    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {

        // Handling touch events
        touchHandler.catchTouchEvent(null, motionEvent);

        return callback.dispatchTouchEvent(motionEvent);
    }

    @Override
    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        return callback.dispatchTrackballEvent(motionEvent);
    }

    @Override
    public boolean dispatchGenericMotionEvent(MotionEvent motionEvent) {
        return callback.dispatchGenericMotionEvent(motionEvent);
    }

    @Override
    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return callback.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }

    @Nullable
    @Override
    public View onCreatePanelView(int i) {
        return null;
    }

    @Override
    public boolean onCreatePanelMenu(int i, Menu menu) {
        return false;
    }

    @Override
    public boolean onPreparePanel(int i, View view, Menu menu) {
        return false;
    }

    @Override
    public boolean onMenuOpened(int i, Menu menu) {
        return false;
    }

    @Override
    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        return false;
    }

    @Override
    public void onWindowAttributesChanged(WindowManager.LayoutParams layoutParams) {

    }

    @Override
    public void onContentChanged() {

    }

    @Override
    public void onWindowFocusChanged(boolean b) {

    }

    @Override
    public void onAttachedToWindow() {

    }

    @Override
    public void onDetachedFromWindow() {

    }

    @Override
    public void onPanelClosed(int i, Menu menu) {

    }

    @Override
    public boolean onSearchRequested() {
        return false;
    }

    @Override
    public boolean onSearchRequested(SearchEvent searchEvent) {
        return false;
    }

    @Nullable
    @Override
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
        return null;
    }

    @Nullable
    @Override
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
        return null;
    }

    @Override
    public void onActionModeStarted(ActionMode actionMode) {

    }

    @Override
    public void onActionModeFinished(ActionMode actionMode) {

    }
}
