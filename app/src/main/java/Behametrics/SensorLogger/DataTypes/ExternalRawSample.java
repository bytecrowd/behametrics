package Behametrics.SensorLogger.DataTypes;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Representing one raw sample measured from sensor
 *
 * @author Imothep
 * @version 0.2.3
 */
public class ExternalRawSample extends AbstractSample {

    private String userId;
    private long timestamp;
    private int sensorType;
    private float[] values;
    private String sensorTypeName;

    public String getSensorTypeName() {
        return sensorTypeName;
    }

    public void setSensorTypeName(String sensorTypeName) {
        this.sensorTypeName = sensorTypeName;
    }

    /**
     * Constructor
     */
    public ExternalRawSample() {
    }

    /**
     * Constructor
     *
     * @param sensorType type of sensor
     * @param timestamp  time of measurement
     * @param values     generic vector of values
     */
    public ExternalRawSample(int sensorType,String sensorTypeName, long timestamp, float[] values) {
        this.values = values;
        this.sensorType = sensorType;
        this.timestamp = timestamp;
        this.sensorTypeName = sensorTypeName;
    }

    /**
     * Sets sensor type
     *
     * @param sensorType integer type
     */
    public void setSensorType(int sensorType) {
        this.sensorType = sensorType;
    }

    /**
     * Sets generic vector of values
     *
     * @param values array of floats type
     */
    public void setValues(float[] values) {
        this.values = values;
    }

    /**
     * Sets timestamp of measurement
     *
     * @param timestamp long type
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Gets type of sensor
     *
     * @return integer type
     */
    public int getSensorType() {
        return sensorType;
    }

    /**
     * Gets generic vector of measured values
     *
     * @return array of floats type
     */
    public float[] getValues() {
        return values;
    }

    /**
     * Gets time of measurement
     *
     * @return long type
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Serializaton into CSV data format
     *
     * @return String representation
     */
    public String serializeToCsv() {
        StringBuilder strOutput = new StringBuilder();
        strOutput.append(sensorType)
                .append(";")
                .append(sensorTypeName)
                .append(";")
                .append(timestamp);

        for (int i = 0; i < values.length; i++) {
            strOutput.append(";");
            strOutput.append(values[i]);
        }

        return strOutput.toString() + "\n";
    }

    /**
     * Serializaton into JSON data format
     *
     * @return String representation
     */
    @Override
    public String serializeToJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("userId", userId)
                    .put("sensorType", sensorType)
                    .put("sensorTypeName", sensorTypeName)
                    .put("timestamp", timestamp);

            for (int i = 0; i < values.length; i++) {
                json.put("value" + i, values[i]);
            }

        } catch (JSONException e) {
          Log.e("JSON", "Serialize to JSON");
        }

        return json.toString();
    }

    /**
     * Debug information about sample
     *
     * @return String representation
     */
    @Override
    public String toString() {
        StringBuilder strOutput = new StringBuilder();
        strOutput.append(sensorType)
                .append(new Date(timestamp))
                .append(" values => ");

        for (int i = 0; i < values.length; i++) {

            strOutput.append(values[i]);

            if ((i + 1) != values.length) {
                strOutput.append(",");
            }
        }

        return strOutput.toString();
    }
}
