package Behametrics.SensorLogger.DataTypes;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Representing one raw sample measured from TouchEvent
 *
 * @author Imothep
 * @version 0.2.3
 */
public class InternalRawSample extends AbstractSample {

    private String userId;
    private long timestamp;
    private int eventType;
    private int touchPointer;
    private float[] values;
    private float pressure;
    private float size;

    public String getEventTypeName() {
        return eventTypeName;
    }

    public void setEventTypeName(String eventTypeName) {
        this.eventTypeName = eventTypeName;
    }

    private String eventTypeName;

    /**
     * Constructor
     */
    public InternalRawSample() {
    }

    /**
     * Constructor
     *
     * @param eventType type of touch event
     * @param timestamp time of measurement
     * @param values    absolute screen coordinates X,Y
     */
    public InternalRawSample(int eventType, long timestamp, float[] values, float pressure) {
        setValues(values);
        setEventType(eventType);
        setTimestamp(timestamp);
        setPressure(pressure);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Sets event type
     *
     * @param eventType integer type
     */
    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    /**
     * Sets screen coordinates
     *
     * @param values array of floats type
     */
    public void setValues(float[] values) {
        this.values = values;
    }

    /**
     * Sets timestamp of measurement
     *
     * @param timestamp long type
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Sets ID of touch pointer
     *
     * @param touchPointer integer type
     */
    public void setTouchPointer(int touchPointer) {
        this.touchPointer = touchPointer;
    }

    /**
     * Sets approximated area of touch event
     *
     * @param size float representation of approximated area
     */
    public void setSize(float size) {this.size = size;}

    /**
     * Sets pressure of touch event
     *
     * @param pressure float representation of pressure
     */
    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    /**
     * Gets touch pressure
     *
     * @return pressure represented in float value
     */
    public float getPressure() {
        return pressure;
    }
    /**
     * Gets approximated touch size
     *
     * @return size represented in float value
     */
    public float getSize() {
        return size;
    }
    /**
     * Gets event type of sample
     *
     * @return integer type
     */
    public int getEventType() {
        return eventType;
    }

    /**
     * Gets measured screen coordinates
     *
     * @return array of floats type
     */
    public float[] getValues() {
        return values;
    }

    /**
     * Gets time of measurement
     *
     * @return long type
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Serializaton into CSV data format
     *
     * @return String representation
     */
    @Override
    public String serializeToCsv() {
        StringBuilder strOutput = new StringBuilder();
        strOutput.append(eventType)
                .append(";")
                .append(eventTypeName)
                .append(";")
                .append(timestamp)
                .append(";")
                .append(touchPointer)
                .append(";")
                .append(pressure)
                .append(";")
                .append(size);


        for (int i = 0; i < values.length; i++) {
            strOutput.append(";");
            strOutput.append(values[i]);
        }

        return strOutput.toString() + "\n";
    }

    /**
     * Serializaton into JSON data format
     *
     * @return String representation
     */
    @Override
    public String serializeToJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("userId", userId)
                    .put("sensorType", eventType)
                    .put("sensorTypeName", eventTypeName)
                    .put("timestamp", timestamp)
                    .put("touchPointer", touchPointer)
                    .put("pressure", pressure)
                    .put("size", size);

            for (int i = 0; i < values.length; i++) {
                json.put("value" + i, values[i]);
            }

        } catch (JSONException e) {
            Log.e("JSON", "Serialize to JSON");
        }

        return json.toString();
    }

    /**
     * Debug information about sample
     *
     * @return String representation
     */
    @Override
    public String toString() {
        StringBuilder strOutput = new StringBuilder();
        strOutput.append(eventType)
                .append(new Date(timestamp))
                .append(" values => ");

        for (int i = 0; i < values.length; i++) {

            strOutput.append(values[i]);

            if ((i + 1) != values.length) {
                strOutput.append(",");
            }
        }

        return strOutput.toString();
    }
}
