package Behametrics.SensorLogger.DataTypes;

/**
 * Abstract class representing generic sample
 *
 * @author Imothep
 * @version 0.2.3
 */
public abstract class AbstractSample {

    public abstract void setUserId(String userId);

    public abstract String serializeToCsv();

    public abstract String serializeToJson();
}
