package Behametrics.SensorLogger;

import android.app.Activity;
import android.content.Context;

import Behametrics.SampleHandler;

/**
 * Interface for External and Internal logger
 * <p>
 * Set of basic functions
 * </p>
 *
 * @author Imothep
 * @version 0.2.3
 */
public interface LoggerInterface {
     void setContext(Activity context);
     void setSampleHandler(SampleHandler sampleHandler);
}
