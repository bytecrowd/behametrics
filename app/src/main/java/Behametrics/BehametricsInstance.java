package Behametrics;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.example.authentication.ServerSocket;
import com.github.nkzawa.emitter.Emitter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import Behametrics.SensorLogger.ExternalLogger;
import Behametrics.SensorLogger.InternalLogger;
import Behametrics.SensorLogger.TouchHandler;
import Behametrics.Utils.Observe;
import Behametrics.Utils.PropertyReader;

/**
 * Class BehametricsInstance contains method for implementing Logger into your custom application.
 */
public class BehametricsInstance {

    private static BehametricsInstance behametricsInstance = null;
    private static InternalLogger internalLogger;
    private static ExternalLogger externalLogger;
    private static SampleHandler sampleHandler;
    private static Activity context;
    private static SharedPreferences wmbPreference;
    private static TouchHandler touchHandler;
    private static PropertyReader propertyReader;
    private static PropertyReader staticPropertyReader;
    private static ServerSocket serverSocket;

    private static List<Observe> observeProperties = new ArrayList<Observe>();

    private BehametricsInstance(Activity context) {
        //set properties for whole project - properties are static
        staticPropertyReader = new PropertyReader(context);
        staticPropertyReader.setProperties("config.properties");
        staticPropertyReader.setProperty("defaultDirName", quoteDirName(staticPropertyReader.getStringProperty("defaultDirName")));

        propertyReader = new PropertyReader(context);
        propertyReader = staticPropertyReader.copy();

        sampleHandler = new SampleHandler(context.getFilesDir().getAbsolutePath(), context, propertyReader);
        touchHandler = new TouchHandler(sampleHandler);

        externalLogger = new ExternalLogger(context, sampleHandler);
        wmbPreference = PreferenceManager.getDefaultSharedPreferences(context);

    }

    /**
     * Creating instance of singleton
     * @param context
     */
    public static void init(Activity context) {
        BehametricsInstance.context=context;
        if (behametricsInstance == null) {
            behametricsInstance = new BehametricsInstance(context);

        } else {
            switchActivity(context);
        }

        Boolean isFirstRun = wmbPreference.getBoolean("FIRSTRUN", true);
        if (isFirstRun) {
            String uniqueDevice = UUID.randomUUID().toString();

            SharedPreferences.Editor editor = wmbPreference.edit();
            editor.putString("UUID", uniqueDevice);
            editor.putBoolean("FIRSTRUN", false);
            editor.commit();
        }

        //Initialize ServerSocket
        if(serverSocket == null){
            changeDirName("DefaultDirectory");
        }
    }

    private static void setServerSocket(String dirName)
    {
        //Initialize ServerSocket
        if(propertyReader.getStringProperty("ModuleAuthentication").equals("True")){
            serverSocket = new ServerSocket(propertyReader.getStringProperty("SocketAddress"),
                    wmbPreference.getString("UUID", null) + "/" + dirName, context, propertyReader.getStringProperty("StreamAddress"));
        }
    }

    /**
     * Function for switching context, if activity changed
     *
     * @param context
     */
    private static void switchActivity(Activity context) {

        externalLogger.setContext(context);
        BehametricsInstance.context=context;
    }

    /**
     * Access current application context
     * @return
     */
    public static Context getContext(){
        return BehametricsInstance.context;
    }

    /**
     * Function for starting logging
     * Create behametricsInstance and start logging
     * @param context
     */
    public static void create(Activity context) {
        init(context);
    }

    /**
     * Function for resume activity
     * @param context
     */
    public static void resume(Activity context) {
        if (behametricsInstance != null)
            switchActivity(context);
    }

    /**
     * Register events for internal logger and sensors for external logger
     */
    public static void start(Activity context) {
        if (behametricsInstance != null) {
            switchActivity(context);

            context.getWindow().setCallback(new InternalLogger(touchHandler, context));
            externalLogger.initLogger();
        }
    }

    /**
     * Close instance of Behametrics loggers
     */
    public static void destroy(Activity context) {

    }

    /**
     * Functionality for changing name of log dir
     *
     * @param dirName new name for log dir
     */
    public static void changeDirName(String dirName) {
        String quotedDirName = quoteDirName(dirName);

        propertyReader.setProperty("defaultDirName", quotedDirName);

        if(serverSocket != null)
        {
            serverSocket.disconnect();
        }

        setServerSocket(quotedDirName);

        behametricsInstance.update();
    }

    /**
     * Functionality for set default dirname
     */
    public static void setDefaultDirName() {
        propertyReader.setProperty("defaultDirName", staticPropertyReader.getStringProperty("defaultDirName"));
        behametricsInstance.update();
    }

    public static String quoteDirName(String val)
    {
        return val.replaceAll("-","--").replaceAll("/","-");
    }

    /**
     * Add to list if some class subscribe to behametricsInstance
     * @param observe
     */
    public static void addToList(Observe observe) {
        observeProperties.add(observe);
    }

    protected void update() {
        for (Observe<PropertyReader> observe: observeProperties) {
            observe.update(propertyReader);
        }
    }

    public static ServerSocket getServerSocket() {
        return serverSocket;
    }

    public static void setServerSocket(ServerSocket serverSocket) {
        BehametricsInstance.serverSocket = serverSocket;
    }

    public static void onAuth(Emitter.Listener listener)
    {
        serverSocket.setAuth(listener);
    }

}
