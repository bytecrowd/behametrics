package Behametrics.Handlers;

import Behametrics.SensorLogger.DataTypes.AbstractSample;

public abstract class AbstractHandler {

    public abstract boolean handle(AbstractSample sample);

}
