package Behametrics.Handlers;

import android.util.Log;

import java.io.IOException;

import Behametrics.SensorLogger.DataTypes.AbstractSample;
import Behametrics.Utils.File.FileUtils;

public class CsvHandler extends AbstractHandler{

    private FileUtils fileUtils;

    public CsvHandler(FileUtils fileUtils){
        this.fileUtils = fileUtils;
    }

    @Override
    public boolean handle(AbstractSample sample){
        try {
            fileUtils.write(sample.serializeToCsv());
        } catch (IOException | OutOfMemoryError e) {
            Log.e("CSVHandler", "Chyba pri zapisovani do suboru: ", e);
            return false;
        }
        return true;
    }

}
