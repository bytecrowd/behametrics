package Behametrics.Handlers;

import Behametrics.SensorLogger.DataTypes.AbstractSample;
import Behametrics.Utils.Network.NetworkUtils;

public class StreamHandler extends AbstractHandler {

    private NetworkUtils networkUtils;

    public StreamHandler(NetworkUtils networkUtils){
        this.networkUtils = networkUtils;
    }

    @Override
    public boolean handle(AbstractSample sample) {
        networkUtils.stream(sample);
        return true;
    }


}
