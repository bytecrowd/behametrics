package sk.stuba.fiit.studenti.team07_17.behametrics;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.annotations.Logger;
import com.example.generated.MainActivityLogger;
import com.github.nkzawa.emitter.Emitter;

import Behametrics.BehametricsInstance;
import xdroid.toaster.Toaster;


@Logger()
public class MainActivity extends AppCompatActivity {

    private AlertDialog.Builder alertDialogBuilder;
    private EditText input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MainActivityLogger.init(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Button btnSwitch = (Button) findViewById(R.id.btnSwitchActivity);
        btnSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent secondIntent = new Intent(MainActivity.this, SecondActivity.class);
                MainActivity.this.startActivity(secondIntent);

            }
        });

        Button btnSetNewDir = (Button) findViewById(R.id.btnDirName);
        btnSetNewDir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                alertForDir();
            }
        });

        Button btnDefaultDirName = (Button) findViewById(R.id.btnDirNameDefault);
        btnDefaultDirName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BehametricsInstance.setDefaultDirName();
            }
        });

        Button btnAuthenticate = (Button)findViewById(R.id.btnAuth);
        btnAuthenticate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BehametricsInstance.getServerSocket().sendAuthenticate();
            }
        });

        Button btnLearn = (Button)findViewById(R.id.btnLearn);
        btnLearn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BehametricsInstance.getServerSocket().sendLearn();
            }
        });

        BehametricsInstance.onAuth(new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i("onAuth", "Response: " + String.valueOf(args[0]));
                Toaster.toast(String.valueOf(args[0]));
            }
        });

    }

    private void alertForDir() {
        alertDialogBuilder = new AlertDialog.Builder(this);
        input = new EditText(this);
        alertDialogBuilder.setTitle("Change dirname");
        alertDialogBuilder.setMessage("Enter new dirname: ");
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        alertDialogBuilder.setView(input);
        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Log.d("Login", "Success");
                String dirName = input.getText().toString();
                BehametricsInstance.changeDirName(dirName);
            }
        });
        alertDialogBuilder.show();
    }
}
