package sk.stuba.fiit.studenti.team07_17.behametrics;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import Behametrics.BehametricsInstance;

/**
 * Created by Peter on 08/Nov/17.
 */

public class SecondActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        BehametricsInstance.create(this);

        Button back = (Button) findViewById(R.id.btnBack);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        BehametricsInstance.start(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        BehametricsInstance.resume(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
