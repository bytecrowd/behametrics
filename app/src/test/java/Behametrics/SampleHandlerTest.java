//package Behametrics;
//
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.rules.TemporaryFolder;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.mockito.runners.MockitoJUnitRunner;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//import Behametrics.SensorLogger.DataTypes.AbstractSample;
//import Behametrics.SensorLogger.DataTypes.ExternalRawSample;
//import Behametrics.Utils.File.FileUtils;
//
//import static org.mockito.Mockito.doNothing;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//@RunWith(MockitoJUnitRunner.class)
//public class SampleHandlerTest {
//
//    @Rule
//    public TemporaryFolder folder = new TemporaryFolder();
//
//    @Mock
//    private FileUtils fileUtils;
//
//    @InjectMocks
//    private SampleHandler sampleHandler;
//
//    @Before
//    public void setUp() throws Exception {
//        String logDir = "behametrics-sensor-logs";
//        File tmpDir = folder.newFolder(logDir);
//        System.out.println(tmpDir.getAbsolutePath());
//        sampleHandler = new SampleHandler(tmpDir.getAbsolutePath());
//        fileUtils = sampleHandler.get();
//        System.out.println(fileUtils);
//        MockitoAnnotations.initMocks(this);
//    }
//
//    @Test
//    public void writeInvocationTest() throws IOException {
//        AbstractSample sample = new ExternalRawSample(1, 2, new float[]{(float) 1.3, (float) 6.3111, (float) 2.444});
//        doNothing().when(fileUtils).write(sample.serializeToCsv());
//        when(fileUtils.existFileToSend()).thenReturn(false);
//        sampleHandler.addSample(sample);
//        verify(fileUtils).write(sample.serializeToCsv());
//    }
//
//    @Test
//    public void writeAllInvocationTest() throws IOException {
//        AbstractSample sample1 = new ExternalRawSample(1, 234242342, new float[]{(float) 1.3, (float) 6.3111, (float) 2.444});
//        AbstractSample sample2 = new ExternalRawSample(2, 432112312, new float[]{(float) 1.3, (float) 6.3111, (float) 2.444});
//        AbstractSample sample3 = new ExternalRawSample(3, 954455432, new float[]{(float) 1.3, (float) 6.3111, (float) 2.444});
//
//        List<AbstractSample> samples = new ArrayList<>();
//        samples.add(sample1);
//        samples.add(sample2);
//        samples.add(sample3);
//
//        when(fileUtils.existFileToSend()).thenReturn(false);
//
//        sampleHandler.addAllSamples(samples);
//        verify(fileUtils).write(sample1.serializeToCsv());
//        verify(fileUtils).write(sample1.serializeToCsv());
//        verify(fileUtils).write(sample3.serializeToCsv());
//    }
//
//    @Test
//    public void closeInvocationTest() throws IOException {
//        sampleHandler.close();
//        verify(fileUtils).close();
//    }
//}
