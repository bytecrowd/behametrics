package Behametrics.Utils.File;

import android.content.Context;
import android.test.mock.MockContext;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import Behametrics.Utils.PropertyReader;
import sk.stuba.fiit.studenti.team07_17.behametrics.MainActivity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by lacht on 28. 10. 2017.
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class FileUtilsTest {

    String logDir;
    File tmpDir;
    PropertyReader propertyReader;

    @Mock
    Context mMockContext = new MockContext();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        logDir = "behametrics-sensor-logs";
        tmpDir = folder.newFolder(logDir);
        for (File file : tmpDir.listFiles()) {
            file.delete();
        }

        propertyReader = new PropertyReader(mock(MainActivity.class, Mockito.RETURNS_DEEP_STUBS));
        propertyReader.setProperties("config.properties");

        propertyReader.setProperty("LogFilesPath", "/" + logDir);
        propertyReader.setProperty("FileMaxRows", "100");
        propertyReader.setProperty("defaultDirName", "Tests");

    }

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void initializationWithFiles() throws IOException {
        BufferedWriter bw1 = new BufferedWriter(new FileWriter(tmpDir.getAbsolutePath() + "/165131531484.csv"));
        bw1.write("Obsah prveho suboru.");
        bw1.close();

        BufferedWriter bw2 = new BufferedWriter(new FileWriter(tmpDir.getAbsolutePath() + "/165131531485.csv"));
        bw2.write("Obsah druheho suboru, ktory by sa mal poslat lebo je najvacsi");
        bw2.close();

        File empty = new File(tmpDir.getAbsolutePath() + "/165131531486.csv");
        empty.createNewFile();

        FileUtils utils = new FileUtils(folder.getRoot().getPath(), propertyReader);

        assertEquals(true, utils.existFileToSend());
        assertEquals(0, utils.getActRowNumber());
        assertEquals(2, utils.getNumOfFilesToSend());
        assertEquals("165131531485.csv", utils.getFileToSend().getName());
    }

    @Test
    public void deleteEmptyFilesAtInit() throws IOException {
       //Create empty files
        new File(tmpDir.getAbsolutePath() + "/165131531486.csv").createNewFile();
        new File(tmpDir.getAbsolutePath() + "/165131531487.csv").createNewFile();
        new File(tmpDir.getAbsolutePath() + "/165131531488.csv").createNewFile();

        assertEquals(3, tmpDir.listFiles().length);
        FileUtils utils = new FileUtils(folder.getRoot().getPath(), propertyReader);
        assertEquals(1, tmpDir.listFiles().length);
    }

    @Test
    public void initializationWithoutFiles() throws IOException {
        FileUtils utils = new FileUtils(folder.getRoot().getPath(), propertyReader);

        assertEquals(false, utils.existFileToSend());
        assertEquals(0, utils.getActRowNumber());
        assertEquals(0, utils.getNumOfFilesToSend());
        assertEquals(null, utils.getFileToSend());
    }

    @Test
    public void delete() throws IOException {
        BufferedWriter bw1 = new BufferedWriter(new FileWriter(tmpDir.getAbsolutePath() + "/165131531484.csv"));
        bw1.write("Obsah prveho suboru.");
        bw1.close();

        BufferedWriter bw2 = new BufferedWriter(new FileWriter(tmpDir.getAbsolutePath() + "/165131531485.csv"));
        bw2.write("Obsah druheho suboru, ktory by sa mal vymazat lebo je najvacsi");
        bw2.close();

        FileUtils utils = new FileUtils(folder.getRoot().getPath(), propertyReader);
        utils.delete();

        assertEquals(true, utils.existFileToSend());
        assertEquals(0, utils.getActRowNumber());
        assertEquals(1, utils.getNumOfFilesToSend());
    }

    @Test
    public void nextFileTest() throws IOException {
        String sample = "1;234242342;1.3;6.3111;2.444;";
        FileUtils utils = new FileUtils(folder.getRoot().getPath(), propertyReader);
        utils.setActRowNumber(100);

        assertEquals(false, utils.existFileToSend());

        utils.write(sample);

        assertEquals(true, utils.existFileToSend());
        assertEquals(0, utils.getActRowNumber());
        assertEquals(1, utils.getNumOfFilesToSend());
    }

    @Test
    public void writeTest() throws IOException {
        String sample = "1;234242342;1.3;6.3111;2.444;";

        FileUtils utils = new FileUtils(folder.getRoot().getPath(), propertyReader);

        assertEquals(0, utils.getActRowNumber());
        utils.write(sample);
        assertEquals(1, utils.getActRowNumber());

        utils.setActRowNumber(15);
        utils.write(sample);
        assertEquals(16, utils.getActRowNumber());
    }

    @Test
    public void close() {
        BufferedWriter writer = mock(BufferedWriter.class);

        FileUtils utils = new FileUtils(writer);

        try {
            utils.close();
            verify(writer).close();
        } catch (IOException e) {
        }
    }
}
