package com.example.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Imothep on 12.02.2018.
 * According to :
 * https://developer.android.com/reference/android/hardware/SensorManager.html
 */
class SensorDelay {
    public static final int SENSOR_DELAY_FASTEST = 0;
    public static final int SENSOR_DELAY_GAME = 1;
    public static final int SENSOR_DELAY_UI = 2;
    public static final int SENSOR_DELAY_NORMAL = 3;
}

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface SensorProperty {
    int SensorType();
    int SensorDelay() default SensorDelay.SENSOR_DELAY_NORMAL;
}
