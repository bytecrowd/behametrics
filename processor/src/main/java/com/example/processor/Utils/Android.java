package com.example.processor.Utils;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;

import java.util.HashMap;

/**
 * Created by Imothep on 26.02.2018.
 */

public class Android {

    private static final HashMap<String, String> classTypes = new HashMap<String, String>(){{

        put("Activity", "android.app");

        put("ActivityLifeTracker", "Behametrics.Callbacks");

    }};

    public static ClassName getClass(String name)
    {
        String packageName = classTypes.get(name);

        return ClassName.get(packageName, name);
    }
}
