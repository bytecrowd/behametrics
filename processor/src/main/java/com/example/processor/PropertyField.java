package com.example.processor;

/**
 * Created by Imothep on 13.02.2018.
 */

public class PropertyField<T> implements IPropertyField<T>
{
    private final Class<T> type;

    @Override
    public Class<T> getType() {
        return type;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    private T value;

    public PropertyField(Class<T> type, T value)
    {
        this.type = type;
        this.value = value;
    }
}
