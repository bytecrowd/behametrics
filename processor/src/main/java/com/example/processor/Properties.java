package com.example.processor;


import com.google.common.collect.BiMap;
import com.google.common.collect.SetMultimap;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.security.KeyPair;
import java.util.HashMap;
import java.util.List;

import javax.lang.model.element.Modifier;

/**
 * Created by Imothep on 12.02.2018.
 */


interface IProperty<N,V>
{
    V put(N name, V value);
    V get(N name);

    boolean has(N name);
    V remove(N name);
}

interface IPropertyField<T>
{
    Class<T> getType();
}



/*
* Create properties class with ImmutableConfig
* */
public class Properties<V> implements IProperty<String,V>{

    HashMap<String, V> attributes;

    @Override
    public V put(String name, V value) {
        return null;
    }

    @Override
    public V get(String name) {
        return null;
    }

    @Override
    public boolean has(String name) {
        return false;
    }

    @Override
    public V remove(String name) {
        return null;
    }

}
