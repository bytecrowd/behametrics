package com.example.processor.ProcessHandler;

import com.example.processor.LoggerProcessor;

import javax.lang.model.element.Element;

/**
 * Created by Imothep on 14.02.2018.
 */

public abstract class IProcessHandler {
    String regIdentifier;

    // Call Super
    public IProcessHandler(String regIdentifier)
    {
        // Make sure that regIdentifier is unique
        this.regIdentifier = regIdentifier;

        // Register yourself for work
        register();
    }

   // Override this in subclasses
   public abstract void process(Element element);

   private void register()
   {

   }
}
